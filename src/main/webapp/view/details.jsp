<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	table,tr,th,td
	{
		border:1px solid black;
		padding:5px;
	}

	table
	{
		border-collapse:collapse;
	}
</style>
</head>
<body bgcolor="black" style="color: white;">

<h1> Employee Details</h1>
	<table>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Organization</th>
		</tr>
		
		<tr>
			<td>${emp.name}</td>
			<td>${emp.email}</td>
			<td>${emp.organization}</td>
		
		</tr>
	</table>
</body>
</html>