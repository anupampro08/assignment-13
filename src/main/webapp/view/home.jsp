<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body bgcolor="black" style="color: white;">
<h1 style="text-align: center;"> Register Employee</h1>
	<form:form action="details" modelAttribute="emp">
		Name: <form:input path="name" required="true"/><br><Br/>
		Email: <form:input path="email" />
		<form:errors path="email"  style="color:red;" cssClass="error"/><br><Br/>
		Organization: <form:input path="organization"/>
		<form:errors style="color:red;" path="organization" cssClass="error"/><br><Br/>
		<input type="Submit" value="Submit">
	</form:form>
</body>
</html>